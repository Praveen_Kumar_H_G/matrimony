package com.hcl.learn.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.UserRegistrationDto;
import com.hcl.learn.entity.UserRegistration;
import com.hcl.learn.exception.ResourceConflictExists;
import com.hcl.learn.exception.ResourceNotFound;
import com.hcl.learn.exception.UnauthorizedUser;
import com.hcl.learn.respository.UserRegistrationRepository;
import com.hcl.learn.service.impl.UserServiceImpl;

@ExtendWith(SpringExtension.class)
public class UserServiceImplTest {
	@InjectMocks
	private UserServiceImpl userServiceImpl;
	@Mock
	private UserRegistrationRepository userRepository;
 
	@Test
	void userRegisteredAlreadyExist() {
		UserRegistration user  = new UserRegistration();
		user.setEmail("Praveen@gmail.com");
		
		Mockito.when(userRepository.findByEmail("Praveen@gmail.com")).thenReturn(Optional.of(user));
		UserRegistrationDto registerDto = new UserRegistrationDto();
		registerDto.setEmail("Praveen@gmail.com");
		assertThrows(ResourceConflictExists.class, () -> userServiceImpl.register(registerDto));
 
	}
 
	@Test
	void userRegisteredSuccess() {
		UserRegistration user = UserRegistration.builder().email("anu@gmail.com").build();
		Mockito.when(userRepository.findByEmail("anu@gmail.com")).thenReturn(Optional.empty());
 
		Mockito.when(userRepository.save(user)).thenReturn(user);
		UserRegistrationDto registerDto = UserRegistrationDto.builder().email("anu@gmail.com").build();
		ApiResponse apiResponse = userServiceImpl.register(registerDto);
 
		assertEquals("Sucessfully register", apiResponse.getMessage());
 
	}
 
	@Test
	void loginSuccess() {
		UserRegistration user = UserRegistration.builder().email("anu@gmail.com").password("Anucy@123").build();
 
		Mockito.when(userRepository.findByEmail("anu@gmail.com")).thenReturn(Optional.of(user));
 
		Mockito.when(userRepository.save(user)).thenReturn(user);
 
		ApiResponse apiResponse = userServiceImpl.login(user.getEmail(), user.getPassword());
		assertEquals("log-in sucessfully", apiResponse.getMessage());
 
	}
 
	@Test
	void loginFailure() {
 
		Mockito.when(userRepository.findByEmail("anu@gmail.com")).thenReturn(Optional.empty());
 
		assertThrows(ResourceNotFound.class, () -> userServiceImpl.login("anu@gmail.com", "546gfg"));
	}
 
	@Test
	void invalidUser() {
		UserRegistration user = UserRegistration.builder().email("anu@gmail.com").password("Cynth@123").build();
		Mockito.when(userRepository.findByEmail("anu@gmail.com")).thenReturn(Optional.of(user));
 
		assertThrows(UnauthorizedUser.class, () -> userServiceImpl.login("anu@gmail.com", "564egy"));
 
	}
 
	@Test
	void logoutSuccess() {
		UserRegistration user = UserRegistration.builder().email("anu@gmail.com").password("Cynth@123").build();
		Mockito.when(userRepository.findByEmail("anu@gmail.com")).thenReturn(Optional.of(user));
		Mockito.when(userRepository.save(user)).thenReturn(user);
		ApiResponse apiResponse = userServiceImpl.logout(user.getEmail());
		assertEquals("logged out successfully", apiResponse.getMessage());
 
	}
 
	@Test
	void logoutFailure() {
		Mockito.when(userRepository.findByEmail("anu@gmail.com")).thenReturn(Optional.empty());
		assertThrows(ResourceNotFound.class, () -> userServiceImpl.logout("anud@gmail.com"));
 
	}
	
}
