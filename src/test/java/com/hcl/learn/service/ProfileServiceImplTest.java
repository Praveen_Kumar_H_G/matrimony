package com.hcl.learn.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.ProfileDto;
import com.hcl.learn.entity.Profile;
import com.hcl.learn.entity.UserRegistration;
import com.hcl.learn.exception.ResourceNotFound;
import com.hcl.learn.exception.UnauthorizedUser;
import com.hcl.learn.respository.ProfileRepository;
import com.hcl.learn.respository.UserRegistrationRepository;
import com.hcl.learn.service.impl.ProfileServiceImpl;

@ExtendWith(SpringExtension.class)
public class ProfileServiceImplTest {
	
	@InjectMocks
	private ProfileServiceImpl profileServiceImpl;
	
	@Mock
	private ProfileRepository profileRepository;
	@Mock
	private UserRegistrationRepository userRegistrationRepository;
	
	@Test
	void testUpdateProfileSucess() {
		UserRegistration user= new UserRegistration();
		user.setEmail("Praveen@gmail.com");
		user.setPassword("Praveen@123");
		user.setLoggedIn(true);
		ProfileDto profileDto = ProfileDto.builder().firstName("Praveen").lastName("Kumar")
				.age(29).caste("OBC").email("Praveen@gmail.com").gender("Male")
				.income(1234.00).lastName("Bangalore").maritalStatus("Married")
				.profession("Software")
				.religion("Hindhu").zodiacSign("NA").build();
		Profile profile = Profile.builder().age(profileDto.getAge()).build();
		Mockito.when(userRegistrationRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		Mockito.when(profileRepository.save(profile)).thenReturn(profile);
		ApiResponse apiResponse = profileServiceImpl.updateProfile(user.getEmail(), profileDto);
		assertEquals("Prfoile update sucessfully...", apiResponse.getMessage());
	}
	
	@Test
	void testUserMismatch() {
		UserRegistration user = UserRegistration.builder().email("Praveen@gmail.com").password("Praveen@123").loggedIn(true).build();
		ProfileDto profileDto = ProfileDto.builder().location("Bangalore").age(29).caste("OBC")
				.email("Praveen1@gmail.com").firstName("Praveen").lastName("kumar").gender("MALE").income(12345)
				.maritalStatus("Married").profession("kjh").religion("HINDU").build();
		Mockito.when(userRegistrationRepository.findByEmail(user.getEmail())).thenReturn(Optional.of(user));
		assertThrows(UnauthorizedUser.class,
				() -> profileServiceImpl.updateProfile("Praveen@gmail.com", profileDto));
	}
	

}
