package com.hcl.learn.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.UserRegistrationDto;
import com.hcl.learn.service.UserService;


@ExtendWith(SpringExtension.class)
class UserControllerTest {
	@InjectMocks
	private UserController userController;
	@Mock
	private UserService userService;
	@Test
	void testregisterSuccess()
	{
		UserRegistrationDto registerDto = UserRegistrationDto.builder().email("anu@gmail.com").password("6gfh").userName("megha").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(201l).build();
		Mockito.when(userService.register(registerDto)).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=userController.register(registerDto);
		assertEquals(201,responseEntity.getBody().getHttpStatus());
	}
	@Test
	void testLogin()
	{
		UserRegistrationDto user = UserRegistrationDto.builder().email("anu@gmail.com").password("Cynth@123").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(200l).build();
		Mockito.when(userService.login(user.getEmail(),user.getPassword())).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity=userController.login(user.getEmail(),user.getPassword());
		assertEquals(200,responseEntity.getBody().getHttpStatus());
	}
	@Test
	void testLogout()
	{
		UserRegistrationDto user = UserRegistrationDto.builder().email("anu@gmail.com").build();
		ApiResponse apiResponse = ApiResponse.builder().httpStatus(200l).build();
		Mockito.when(userService.logout(user.getEmail())).thenReturn(apiResponse);
		ResponseEntity<ApiResponse> responseEntity =userController.logout(user.getEmail());
		assertEquals(200,responseEntity.getBody().getHttpStatus());

	}
}
