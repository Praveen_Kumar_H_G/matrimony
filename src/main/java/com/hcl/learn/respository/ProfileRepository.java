package com.hcl.learn.respository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.learn.entity.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	Profile findByEmail(String email);

	List<Profile> findByGenderIgnoreCaseAndAgeBetweenAndMaritalStatusIgnoreCaseAndReligionIgnoreCaseAndCasteIgnoreCase(
			String gender, int ageFrom, int ageTo, String maritalStatus, String religion, String caste);

	List<Profile> findByGender(String gender);


}
