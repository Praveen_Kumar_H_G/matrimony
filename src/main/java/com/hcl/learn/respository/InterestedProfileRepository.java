package com.hcl.learn.respository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.learn.entity.InterestedProfile;

public interface InterestedProfileRepository extends JpaRepository<InterestedProfile, Long> {

}
