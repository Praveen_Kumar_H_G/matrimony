package com.hcl.learn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.UserRegistrationDto;
import com.hcl.learn.service.UserService;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

@RestController
@RequestMapping("/api/users")
public class UserController {
	private final UserService userService;

	@Autowired
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@PostMapping("/register")
	public ResponseEntity<ApiResponse> register(@Valid @RequestBody UserRegistrationDto userRegisterDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(userService.register(userRegisterDto));
	}

	@PostMapping("/login")
	public ResponseEntity<ApiResponse> login(
			@Valid @Email(message = "invalid email") @NotBlank @RequestParam String email,
			@RequestParam String password) {
		return ResponseEntity.status(HttpStatus.OK).body(userService.login(email, password));
	}

	@PostMapping("/logout/{email}")
	public ResponseEntity<ApiResponse> logout(
			@Valid @Email(message = "invalid email") @NotBlank @PathVariable String email) {
		return ResponseEntity.status(HttpStatus.OK).body(userService.logout(email));
	}
	
	


}
