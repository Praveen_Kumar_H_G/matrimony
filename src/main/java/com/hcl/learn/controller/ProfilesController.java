package com.hcl.learn.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.InterstedProfileDto;
import com.hcl.learn.dto.ProfileDto;
import com.hcl.learn.entity.Profile;
import com.hcl.learn.service.ProfileService;

import jakarta.validation.Valid;

@RestController
@RequestMapping("/profile")
public class ProfilesController {
	private final ProfileService profileService;

	@Autowired
	public ProfilesController(ProfileService profileService) {
		super();
		this.profileService = profileService;
	}

	@PutMapping("/{email}")
	public ResponseEntity<ApiResponse> updateProfile(@PathVariable String email,
			@Valid @RequestBody ProfileDto profileDto) {
		return ResponseEntity.status(HttpStatus.OK).body(profileService.updateProfile(email, profileDto));
	}

	@GetMapping("/{gender}")
	public  List<Profile> findProfileByGender(@RequestParam String gender){
		return profileService.findProfileByGender(gender);
	}
	
	@GetMapping("/{email}")
	public ResponseEntity<List<ProfileDto>> getMatches(@PathVariable String email, @RequestParam String gender,
			@RequestParam int ageFrom, @RequestParam int ageTo, @RequestParam String maritalStatus,
			@RequestParam String religion, @RequestParam String caste) {
		return ResponseEntity.status(HttpStatus.OK)
				.body(profileService.getMatches(email, gender, ageFrom, ageTo, maritalStatus, religion, caste));
	}
	
	@PostMapping("/intrested-profiles/{email}")
	public ResponseEntity<ApiResponse> showInterest(@PathVariable String email,
			@RequestBody InterstedProfileDto interestedProfile) {
		return ResponseEntity.status(HttpStatus.OK).body(profileService.showInterest(email, interestedProfile));
	}


}
