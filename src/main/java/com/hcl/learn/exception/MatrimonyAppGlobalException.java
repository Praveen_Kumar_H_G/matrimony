package com.hcl.learn.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatrimonyAppGlobalException extends RuntimeException{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1;
	private String message;
    private HttpStatus httpStatus;

    public MatrimonyAppGlobalException(String message){
        super(message);
    }
}