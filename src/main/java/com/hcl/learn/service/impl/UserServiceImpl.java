package com.hcl.learn.service.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.UserRegistrationDto;
import com.hcl.learn.entity.UserRegistration;
import com.hcl.learn.exception.ResourceConflictExists;
import com.hcl.learn.exception.ResourceNotFound;
import com.hcl.learn.exception.UnauthorizedUser;
import com.hcl.learn.respository.UserRegistrationRepository;
import com.hcl.learn.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final UserRegistrationRepository userRegistrationRepository;

	@Autowired
	public UserServiceImpl(UserRegistrationRepository userRegistrationRepository) {
		super();
		this.userRegistrationRepository = userRegistrationRepository;
	}

	@Override
	public ApiResponse register(UserRegistrationDto userRegisterDto) {
		Optional<UserRegistration> byEmail = userRegistrationRepository.findByEmail(userRegisterDto.getEmail());
		if (byEmail.isPresent()) {
			throw new ResourceConflictExists("User already register");
		}
		UserRegistration user = new UserRegistration();
		user.setEmail(userRegisterDto.getEmail());
		user.setPassword(userRegisterDto.getPassword());
		user.setUserName(userRegisterDto.getUserName());
		userRegistrationRepository.save(user);

		ApiResponse api = new ApiResponse();
		api.setHttpStatus(200l);
		api.setMessage("Sucessfully register");
		return api;
	}

	@Override
	public ApiResponse login(String email, String password) {
		UserRegistration registration = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFound("USer Not Found"));
		if (registration.getPassword().equals(password)) {
			registration.setLoggedIn(true);
			userRegistrationRepository.save(registration);

			ApiResponse api = new ApiResponse();
			api.setHttpStatus(200l);
			api.setMessage("log-in sucessfully");
			return api;

		} else {
			throw new UnauthorizedUser("Unauthorized user");
		}
	}

	@Override
	public ApiResponse logout(String email) {
		UserRegistration registration = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFound("User Not Exits"));
		registration.setLoggedIn(false);
		userRegistrationRepository.save(registration);

		ApiResponse api = new ApiResponse();
		api.setHttpStatus(200l);
		api.setMessage("logged out successfully");
		return api;
	}

}
