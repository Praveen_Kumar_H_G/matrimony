package com.hcl.learn.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.InterstedProfileDto;
import com.hcl.learn.dto.ProfileDto;
import com.hcl.learn.entity.InterestedProfile;
import com.hcl.learn.entity.Profile;
import com.hcl.learn.entity.Status;
import com.hcl.learn.entity.UserRegistration;
import com.hcl.learn.exception.ResourceConflictExists;
import com.hcl.learn.exception.ResourceNotFound;
import com.hcl.learn.exception.UnauthorizedUser;
import com.hcl.learn.respository.InterestedProfileRepository;
import com.hcl.learn.respository.ProfileRepository;
import com.hcl.learn.respository.UserRegistrationRepository;
import com.hcl.learn.service.ProfileService;

import jakarta.transaction.Transactional;
import jakarta.validation.Valid;

@Service
public class ProfileServiceImpl implements ProfileService {

	private final ProfileRepository profileRepository;
	private final UserRegistrationRepository userRegistrationRepository;
	private final InterestedProfileRepository interestedProfileRepository;

	private static final String EXCEPTION_MSG = "User Not Found";

	@Autowired
	public ProfileServiceImpl(ProfileRepository profileRepository,
			UserRegistrationRepository userRegistrationRepository,
		InterestedProfileRepository interestedProfileRepository) {
		super();
		this.profileRepository = profileRepository;
		this.userRegistrationRepository = userRegistrationRepository;
		this.interestedProfileRepository = interestedProfileRepository;
	}

	@Override
	public ApiResponse updateProfile(String email, @Valid ProfileDto profileDto) {
		UserRegistration user = userRegistrationRepository.findByEmail(email)
				.orElseThrow(() -> new ResourceNotFound(EXCEPTION_MSG));
		if (user.isLoggedIn()) {
			if (user.getEmail().equals(profileDto.getEmail())) {
				Profile prof = new Profile();
				prof.setFirstName(profileDto.getFirstName());
				prof.setLastName(profileDto.getLastName());
				prof.setFirstName(profileDto.getFirstName());
				prof.setGender(profileDto.getGender());
				prof.setReligion(profileDto.getReligion());
				prof.setMaritalStatus(profileDto.getMaritalStatus());
				prof.setCaste(profileDto.getCaste());
				prof.setEmail(profileDto.getEmail());
				prof.setAge(profileDto.getAge());
				prof.setIncome(profileDto.getIncome());
				prof.setProfession(profileDto.getProfession());
				prof.setZodiacSign(profileDto.getZodiacSign());
				prof.setLocation(profileDto.getLocation());

				profileRepository.save(prof);

				ApiResponse api = new ApiResponse();
				api.setHttpStatus(200l);
				api.setMessage("Prfoile update sucessfully...");

				return api;
			} else
				throw new UnauthorizedUser("Unauthorized user");

		} else
			throw new UnauthorizedUser("Login to update the profile");

	}

	@Override
	public List<Profile> findProfileByGender(String gender) {
		return profileRepository.findByGender(gender);

	}

	@Override
	public List<ProfileDto> getMatches(String email, String gender, int ageFrom, int ageTo, String maritalStatus,
			String religion, String caste) {
		UserRegistration user = userRegistrationRepository.findByEmail(email).orElseThrow(()-> new ResourceNotFound(EXCEPTION_MSG));

		if(user.isLoggedIn()) {
			List<Profile> profile = profileRepository.findByGenderIgnoreCaseAndAgeBetweenAndMaritalStatusIgnoreCaseAndReligionIgnoreCaseAndCasteIgnoreCase(
					gender, ageFrom, ageTo, maritalStatus, religion, caste);
			 Profile profile2 = profileRepository.findByEmail(email);
			 return	profile.stream().map(pr-> ProfileDto.builder().firstName(pr.getFirstName())
					.lastName(pr.getLastName()).gender(pr.getGender())
					.caste(pr.getCaste())
					.religion(pr.getReligion())
					.maritalStatus(pr.getMaritalStatus())
					.income(pr.getIncome())
					.location(pr.getLocation())
					.profession(pr.getProfession())
					.zodiacSign(pr.getZodiacSign()).build()).toList();
		}else
			throw new UnauthorizedUser("Login to view the matches");		
	}
	
	@Override
	@Transactional
	public ApiResponse showInterest(String email, InterstedProfileDto interestedProfileDto) {
		UserRegistration user = userRegistrationRepository.findByEmail(email).orElseThrow(() -> new ResourceNotFound(EXCEPTION_MSG));
		if (user.isLoggedIn()) {
			List<InterestedProfile> interestedProfiles = interestedProfileDto.getEmails().stream()
					.map(interestedProfile -> {
						if (user.getEmail().equals(interestedProfile)) {
							throw new ResourceConflictExists("add favours to self is not valid ");
						}
						return InterestedProfile.builder().userEmail(email).interestedEmail(interestedProfile)
								.status(Status.NORESPONSE).build();
					}).toList();
			interestedProfileRepository.saveAll(interestedProfiles);
			return ApiResponse.builder().message("Profiles Added to interested").httpStatus(201l).build();
		}

		else
			throw new UnauthorizedUser("Login to add Profile favours");
	}

}
