package com.hcl.learn.service;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.UserRegistrationDto;

import jakarta.validation.Valid;

public interface UserService {

	ApiResponse register(UserRegistrationDto userRegisterDto);

	ApiResponse login(String email, String password);

	ApiResponse logout(String email);

}
