package com.hcl.learn.service;

import java.util.List;

import com.hcl.learn.dto.ApiResponse;
import com.hcl.learn.dto.InterstedProfileDto;
import com.hcl.learn.dto.ProfileDto;
import com.hcl.learn.entity.Profile;

public interface ProfileService {

	ApiResponse updateProfile(String email, ProfileDto profileDto);

	List<Profile> findProfileByGender(String gender);

	List<ProfileDto> getMatches(String email, String gender, int ageFrom, int ageTo, String maritalStatus,
			String religion, String caste);


	ApiResponse showInterest(String email, InterstedProfileDto interestedProfile);
	
	
}
