package com.hcl.learn.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProfileDto {
	
	@NotBlank(message = "firstName required")
	@Size(max=10,min=5,message="criteria not met")
	private String firstName;
	private String lastName;
	private String gender;
	private String religion;
	private String maritalStatus;
	@NotBlank(message = "caste required")
	private String caste;
	@Email(message = "invalid email")
	@NotBlank
	private String email;
	@Min(value = 18,message = "age must be greater than or equal to 18")
	private int age;
	private double income;
	private String zodiacSign;
	private String profession;
	@Valid
	private String location;
	

}
