package com.hcl.learn.entity;

import java.time.LocalDateTime;

import com.hcl.learn.dto.ApiResponse;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Profile {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long profileId;
	private String firstName;
	private String lastName;
	private String gender;
	private String religion;
	private String maritalStatus;
	private String caste;
	private String email;
	private int age;
	private double income;
	private String location;
	private String zodiacSign;
	private String profession;
//	@OneToOne
//	@JoinColumn(name="userId")
//	private UserRegistration userRegistration;

}
